Yii2 RBAC
==========

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run
```
php composer.phar require code2magic/yii2-rbac --prefer-dist
```
or add
```
"code2magic/yii2-rbac": "*"
```

to the `require` section of your `composer.json` file.

## Configuration

This extension is supposed to be used with [composer-config-plugin].

Else look files for cofiguration example:

* [src/backend/config/web.php]

[composer-config-plugin]:           https://github.com/hiqdev/composer-config-plugin
[src/backend/config/web.php]:       src/backend/config/web.php

## Usage
