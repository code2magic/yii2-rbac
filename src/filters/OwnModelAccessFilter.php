<?php

namespace code2magic\rbac\filters;

use Yii;
use yii\base\ActionFilter;
use yii\web\ForbiddenHttpException;

/**
 * Class OwnModelAccessFilter
 */
class OwnModelAccessFilter extends ActionFilter
{
    /**
     * @var string Model class name
     */
    public $modelClass;
    /**
     * @var string Primary key param name
     */
    public $modelPkParam = 'id';
    /**
     * @var string Created by attribute name
     */
    public $modelCreatedByAttribute;

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function beforeAction($action)
    {
        $modelPk = Yii::$app->request->getQueryParam($this->modelPkParam);
        if ($modelPk) {
            $model = \Yii::$container->invoke([$this->modelClass, 'findOne'], [$modelPk]);
            if ($model) {
                $isAllowed = Yii::$app->user->can('editOwnModel', [
                    'model' => $model,
                    'attribute' => $this->modelCreatedByAttribute
                ]);
                if (!$isAllowed) {
                    throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
                }
            }
        }
        return true;
    }
}
