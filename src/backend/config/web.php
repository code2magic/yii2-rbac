<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
return [
    'container' => [
        'definitions' => [
        ],
        'singletons' => [
        ],
    ],
    'modules' => [
        'rbac' => [
            'class' => \code2magic\rbac\backend\Module::class,
            'defaultRoute' => 'rbac-auth-item/index',
        ],
    ],
];
