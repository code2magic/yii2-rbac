<?php
/**
 * @var $this yii\web\View
 * @var $model code2magic\rbac\backend\models\RbacAuthAssignment
 * @var $form yii\bootstrap\ActiveForm
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="rbac-auth-assignment-form">
    <?php $form = ActiveForm::begin() ?>
    <?= $form->errorSummary($model); ?>
    <?= $form->field($model, 'item_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'user_id')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'created_at')->textInput() ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
