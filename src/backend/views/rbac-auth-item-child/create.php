<?php
/**
 * @var $this yii\web\View
 * @var $model code2magic\rbac\backend\models\RbacAuthItemChild
 */

use yii\helpers\Html;

$this->title = Yii::t('frontend', 'Create Rbac Auth Item Child');
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Rbac Auth Item Children'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rbac-auth-item-child-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
