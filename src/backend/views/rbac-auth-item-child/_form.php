<?php
/**
 * @var $this yii\web\View
 * @var $model code2magic\rbac\backend\models\RbacAuthItemChild
 * @var $form yii\widgets\ActiveForm
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="rbac-auth-item-child-form">
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'parent')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'child')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('frontend', 'Create') : Yii::t('frontend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
